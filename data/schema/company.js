var mongoose = require("mongoose");

const CompanySchema = new mongoose.Schema({
  name: { type: String, required: true },
  code: { type: String, required: true },
  phoneNumber: { type: String },
  street: { type: String, required: true },
  city: { type: String, required: true },
  state: { type: String, required: true },
  country: { type: String, required: true },
  latitude: { type: Number },
  longitude: { type: Number },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

const Company = mongoose.model("Company", CompanySchema);

mongoose.connect("mongodb://172.17.0.2:27017/salesfalcon", {
  useNewUrlParser: true
});

Company.find(
  {
    code: "Techno"
  },
  (err, doc) => {
    console.log(doc);
  }
);
