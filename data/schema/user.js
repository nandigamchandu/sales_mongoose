const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;

const UserSchema = mongoose.Schema({
  fullName: { type: String, required: true },
  companyId: { type: ObjectId, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  roleId: { type: ObjectId, required: true },
  managerId: { type: ObjectId },
  mobileNumber: { type: String },
  designation: { type: String },
  avatar: { type: String },
  department: { type: String },
  createdAt: {type: Date, default:Date.now},
  updatedAt: {type: Date, default:Date.now}
});

const User = mongoose.model('User', UserSchema)



mongoose.connect("mongodb://172.17.0.2:27017/salesfalcon", { useNewUrlParser: true });

User.find({fullName: "Srinu"}, (err, doc)=>{
    console.log(doc)
})
