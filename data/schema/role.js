const mongoose = require("mongoose");

const RoleSchema = mongoose.Schema({
  name: { type: String, required: true },
  isActive: { type: Boolean, required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

const Role = mongoose.model("Role", RoleSchema);

mongoose.connect("mongodb://172.17.0.2:27017/salesfalcon", {
  useNewUrlParser: true
});

Role.find(
  {
    name: "Admin"
  },
  (err, doc) => {
    console.log(doc);
  }
);
