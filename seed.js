const { ObjectId } = require('mongodb')
const { MongoClient } = require("mongodb");
const { hash } = require('bcryptjs')

const seedData = async (db, url) => {
    const client = db(url)
    await client.connect()
    const database = client.db('salesfalcon')
    const rolesRowCount = await database.collection('roles').countDocuments()
    const companiesRowCount = await database
        .collection('companies')
        .countDocuments()
    const metaTableRowCount = await database
        .collection('metatable')
        .countDocuments()
    const usersRowCount = await database.collection('users').countDocuments()
    let roles = null
    let company = null
    if (rolesRowCount === 0) {
        roles = await database.collection('roles').insertMany([
            {
                name: 'Admin',
                isActive: true,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                name: 'Supervisor',
                isActive: true,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                name: 'SalesAgent',
                isActive: true,
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ])
    }
    if (companiesRowCount === 0) {
        company = await database.collection('companies').insertOne({
            name: 'TechnoIdentity',
            code: 'Techno',
            phoneNumber: '900000000',
            street: 'Kphb Phase 2',
            city: 'Hyderabad',
            state: 'Telangana',
            country: 'India',
            latitude: 17.4757,
            longitude: 78.380852,
            isActive: true,
            createdAt: new Date(),
            updatedAt: new Date(),
        })
    }
    if (usersRowCount === 0) {
        const passwordHash = await hash('test1234567', 10)
        const adminId = roles.ops.find(r => r.name === 'Admin')._id
        await database.collection('users').insertOne({
            fullName: 'Admin',
            companyId: new ObjectId(company.ops[0]._id),
            email: 'admin@gmail.com',
            password: passwordHash,
            roleId: new ObjectId(adminId),
            mobileNumber: '900000000',
            designation: 'Admin',
            department: 'Administration',
            isActive: true,
            createdAt: new Date(),
            updatedAt: new Date(),
        })
        await database.collection('users').insertOne({
            fullName: 'Srinu',
            companyId: new ObjectId(company.ops[0]._id),
            email: 'srinu@gmail.com',
            password: passwordHash,
            roleId: new ObjectId(roles.ops.find(r => r.name === 'Supervisor')._id),
            mobileNumber: '900000000',
            designation: 'Supervisor',
            department: 'IT',
            isActive: true,
            createdAt: new Date(),
            updatedAt: new Date(),
        })
        await database.collection('users').insertOne({
            fullName: 'Vamsi',
            companyId: new ObjectId(company.ops[0]._id),
            email: 'vamsi@gmail.com',
            password: passwordHash,
            roleId: new ObjectId(roles.ops.find(r => r.name === 'SalesAgent')._id),
            mobileNumber: '900000000',
            designation: 'Supervisor',
            department: 'IT',
            isActive: true,
            createdAt: new Date(),
            updatedAt: new Date(),
        })
    }
    if (metaTableRowCount === 0) {
        await database.collection('metatable').insertMany([
            {
                module: 'customer',
                requireFields: [
                    {
                        label: 'Name',
                        fieldName: 'name',
                        fieldType: 'string',
                        type: 'simple',
                        isRequired: true,
                        min: { value: 1 },
                    },
                    {
                        label: 'Customer Type',
                        fieldName: 'customerType',
                        fieldType: 'string',
                        type: 'simple',
                        isRequired: true,
                        min: { value: 1 },
                    },
                    {
                        label: 'Contact Person',
                        fieldName: 'contactPerson',
                        fieldType: 'string',
                        type: 'simple',
                        isRequired: true,
                    },
                ],
            },
            {
                module: 'opportunity',
                requireFields: [
                    {
                        label: 'Opportunity Name',
                        fieldName: 'opportunityName',
                        fieldType: 'string',
                        type: 'simple',
                        isRequired: true,
                        min: { value: 5, message: 'minimum 5 characters required' },
                    },
                    {
                        label: 'Customer',
                        fieldName: 'customerType',
                        fieldType: 'string',
                        type: 'simple',
                        isRequired: true,
                        min: { value: 5, message: 'minimum 5 characters required' },
                    },
                ],
            },
            {
                module: 'product',
                requireFields: [
                    {
                        label: 'product Name',
                        fieldName: 'name',
                        fieldType: 'string',
                        type: 'simple',
                        isRequired: true,
                        min: { value: 5, message: 'minimum 5 characters required' },
                    },
                ],
            },
        ])
    }
    await client.close()
}



const dbClient = (url = "mongodb://172.17.0.2:27017/myproject") =>
  new MongoClient(url, {
    useNewUrlParser: true
  });




seedData(dbClient, "mongodb://172.17.0.2:27017/myproject")
  .then(() => {
    console.log("--------DONE SEEDING--------");
    process.exit(0);
  })
  .catch(error => {
    console.log("error while seeding", error);
    process.exit(1);
  });

module.exports = seedData
